# Ideapad mode - Lenovo

![](https://i.ibb.co/pjmXgZb/ideapadm.png)

At the moment the extension only provides an easy and user-friendly way to toggle the battery conservation mode available on Levono Ideapad laptops and visually get its current state.

# Installation
extensions.gnome.org - https://extensions.gnome.org/extension/4331/ideapad-mode/

Manually download or clone the repository under ~/.local/share/gnome-shell/extensions/ideapad-mode@annexhack.inceptive.ru

**Test for Lenovo Yoga Slim 7 15IIL05**

# Implemented functionality

- Conservation mode
- Fn lock
- Camera power
- Usb charging
- Touchpad

# Installation

```
cd ~/
git clone https://gitlab.com/annexhack/conservation-mode-lenovo
cd conservation-mode-lenovo
make all
rm -r ~/conservation-mode-lenovo
reboot
```

# Languages

- English
- Russian
- French